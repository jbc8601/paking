# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    enable_tax_pakke = fields.Boolean(
        string="Enable tax Pakke",
        config_parameter='delivery_pakke.enable_tax_pakke'
    )
    tax_pakke = fields.Float(
        related='company_id.tax_pakke',
        string="Tax Pakke",
        readonly=False
    )
