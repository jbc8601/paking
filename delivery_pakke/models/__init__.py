# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import inherit_delivery_carrier
from . import inherit_product_packaging
from . import pakke_request
from . import inherit_stock_picking
from . import pakke_shipment
from . import inherit_sale
from . import inherit_product
from . import couriers_pakke
from . import inherit_res_partner
from . import inherit_res_company
# from . import inherit_res_config_settings
