# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class ProductPackaging(models.Model):
    _inherit = 'product.packaging'
    _order = 'volume'

    def _get_default_width_uom(self):
        return self.env['product.template']._get_hlw_uom_name_from_ir_config_parameter().name

    package_carrier_type = fields.Selection(
        selection_add=[('pakke', 'Pakke')]
    )
    height = fields.Float(
        string='Height'
    )
    width = fields.Float(
        string='Width'
    )
    length = fields.Float(
        string='Length'
    )
    volume = fields.Float(
        string='Volume',
        compute='_compute_volume',
        store=True
    )

    height_with_margin = fields.Float(
        string='Height with Margin',
        compute='_compute_height_with_margin'
    )
    margin_side_t_height = fields.Float(
        string="Margin Height Side Top"
    )
    margin_side_b_height = fields.Float(
        string="Margin Height Side Bottom"
    )

    width_with_margin = fields.Float(
        string='Width',
        compute='_compute_width_with_margin'
    )
    margin_side_a_width = fields.Float(
        string="Margin Width Side A"
    )
    margin_side_b_width = fields.Float(
        string="Margin Width Side B"
    )

    length_with_margin = fields.Float(
        string='Length',
        compute='_compute_length_with_margin'
    )
    margin_side_a_length = fields.Float(
        string="Margin Length Side A"
    )
    margin_side_b_length = fields.Float(
        string="Margin Length Side B"
    )

    width_uom_name = fields.Char(compute='_compute_width_uom_name',
                                  default=_get_default_width_uom)

    def _compute_width_uom_name(self):
        for packaging in self:
            packaging.width_uom_name = self.env['product.template']._get_hlw_uom_name_from_ir_config_parameter().name

    @api.onchange('height', 'width', 'length')
    @api.depends('height', 'width', 'length')
    def _compute_volume(self):
        for rec in self:
            rec.volume = rec.height * rec.length * rec.width

    @api.onchange('height', 'margin_side_t_height', 'margin_side_b_height')
    @api.depends('height', 'margin_side_t_height', 'margin_side_b_height')
    def _compute_height_with_margin(self):
        for rec in self:
            rec.height_with_margin = rec.height - rec.margin_side_t_height \
                                     - rec.margin_side_b_height

    @api.onchange('width', 'margin_side_a_width', 'margin_side_b_width')
    @api.depends('width', 'margin_side_a_width', 'margin_side_b_width')
    def _compute_width_with_margin(self):
        for rec in self:
            rec.width_with_margin = rec.width - rec.margin_side_a_width \
                                     - rec.margin_side_b_width

    @api.onchange('length', 'margin_side_a_length', 'margin_side_b_length')
    @api.depends('length', 'margin_side_a_length', 'margin_side_b_length')
    def _compute_length_with_margin(self):
        for rec in self:
            rec.length_with_margin = rec.width - rec.margin_side_a_length \
                                    - rec.margin_side_b_length
