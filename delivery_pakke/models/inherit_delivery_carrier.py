# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import UserError
from odoo.tools import pdf

from .pakke_request import PakkeRequest


class ProviderPakke(models.Model):
    _inherit = 'delivery.carrier'

    delivery_type = fields.Selection(
        selection_add=[('pakke', "Pakke")]
    )
    pakke_api_key = fields.Char(
        string='Api key'
    )
    url_pakke = fields.Char(
        string="Url"
    )
    build_package = fields.Boolean(
        string='Build package',
        help='If it is checked, the packages are built dynamically, if not, '
             'the products are sent as they are.',
        default=False
    )

    def _compute_can_generate_return(self):
        super(ProviderPakke, self)._compute_can_generate_return()
        for carrier in self:
            if carrier.delivery_type == 'pakke':
                carrier.can_generate_return = True

    def pakke_rate_shipment(self, order):
        if order.courier_id:
            return {
                'success': True,
                'price': order.total_price_courier,
                'error_message': False,
                'warning_message': False}
        else:
            return {
                'success': False,
                'price': 0.0,
                'error_message': _('Select the courier to calculate cost.'),
                'warning_message': False
            }

    def pakke_send_shipping(self, pickings):
        return super(ProviderPakke, self).send_shipping(pickings)

    def pakke_get_return_label(self, picking, tracking_number=None,
                               origin_date=None):
        pass
