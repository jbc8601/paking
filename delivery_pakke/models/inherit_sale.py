# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, exceptions
from operator import itemgetter
from .pakke_request import PakkeRequest


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    packaging_ids = fields.Many2many(
        'sale.order.packaging.line',
        'sale_order_pack_rel',
        'order_id',
        'pack_id',
        string='Packagings',
        help='Packagings ready to send to Pakke.'
    )
    courier_id = fields.Char(
        string='CourierId',
        help='Courier Id choiced in quotation and necessary to create a guide.'
    )
    courier_code = fields.Char(
        string="Courier code"
    )
    courier_name = fields.Char()
    total_price_courier = fields.Float()
    shipment_ids = fields.One2many(
        'pakke.shipment',
        'order_id',
        string='Shipments'
    )

    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        if self.carrier_id and self.carrier_id.delivery_type == 'pakke':
            url = self.carrier_id.url_pakke
            api_key = self.carrier_id.pakke_api_key
            request = PakkeRequest(url, api_key)
            for pack in self.packaging_ids:
                courier = {
                    'CourierServiceId': self.courier_id,
                    'CourierCode': self.courier_code,
                }
                order_id = self
                company_id = self.company_id
                partner_id = self.partner_id
                product_id = pack

                shipment = request._create_guide(courier, product_id, order_id,
                                                 company_id, partner_id)
                if 'statusCode' in shipment:
                    raise exceptions.UserError(shipment.get('message'))
                else:
                    name = shipment.get('ShipmentId')
                    order_id = order_id.id
                    reseller_id = shipment.get('ResellerId')
                    owner_id = shipment.get('OwnerId')
                    created_at_array = shipment.get('CreatedAt').split('T')
                    created_at_array[1] = created_at_array[1][0:8]
                    created_at = ' '.join(created_at_array)
                    expire_at_array = shipment.get('ExpiresAt').split('T')
                    expire_at_array[1] = expire_at_array[1][0:8]
                    expire_at = ' '.join(expire_at_array)
                    courier_name = shipment.get('CourierName')
                    courier_code = shipment.get('CourierCode')
                    courier_service_id = shipment.get('CourierServiceId')
                    courier_service = shipment.get('CourierService')
                    length = shipment.get('Parcel').get('Length')
                    width = shipment.get('Parcel').get('Width')
                    height = shipment.get('Parcel').get('Height')
                    weight = shipment.get('Parcel').get('Weight')
                    total_amount = shipment.get('TotalAmount', 0.0)
                    estimated_delivery_date = shipment.get('EstimatedDeliveryDate')
                    tracking_number = shipment.get('TrackingNumber')
                    tracking_status = shipment.get('TrackingStatus')
                    status = shipment.get('Status')
                    file = shipment.get('Label')
                    label = tracking_number + ".pdf"

                    values = {
                        'name': name,
                        'order_id': order_id,
                        'reseller_id': reseller_id,
                        'owner_id': owner_id,
                        'created_at': created_at,
                        'expire_at': expire_at,
                        'courier_name': courier_name,
                        'courier_code': courier_code,
                        'courier_service_id': courier_service_id,
                        'courier_service': courier_service,
                        'length': length,
                        'width': width,
                        'height': height,
                        'weight': weight,
                        'total_amount': total_amount,
                        'estimated_delivery_date': estimated_delivery_date,
                        'tracking_number': tracking_number,
                        'tracking_status': tracking_status.lower(),
                        'status': status.lower(),
                        'file': file,
                        'label': label
                    }
                    shipment_it = self.env['pakke.shipment'].create(values)
                    pack.shipment_id = shipment_it.id
        return res

    def search_packgagin(self, width, height, length):
        domain = [
            ('package_carrier_type', '=', 'pakke'),
            ('height_with_margin', '>=', height),
            ('width_with_margin', '>=', width),
            ('length_with_margin', '>=', length),
        ]
        packagin = self.env['product.packaging'].search(domain,
                                        order='volume ASC', limit=1)
        return packagin

    def compute_variant_1(self, rec):
        """
        Variante 1 donde los productos se envían en cajas y los accesorios se
        van acomodando en mientras quepan en las cajas de los prodcutos, empe
        zando por la más grande, si recorre todas las cajas y quedan accesorios
        pendientes por meter en caja, entonces busca la mas pequeña y la extiende
        :param rec:
        :return:
        """
        variant_1 = 1
        pack_list = []
        # buscamos los productos del pedido
        products = rec.mapped('order_line').filtered(
            lambda x: not x.product_id.accessory_ok and
                      x.product_id.type == 'product')
        for line in products:
            cant = line.product_uom_qty
            while cant > 0:
                quantity = 1
                width = line.product_id.width
                height = line.product_id.height
                length = line.product_id.length
                # buscamos la caja ideal para el producto
                packgagin = self.search_packgagin(width, height, length)
                res_width = packgagin.width_with_margin - width if packgagin else 0.0
                res_height = packgagin.height_with_margin - height if packgagin else 0.0
                res_length = packgagin.length_with_margin - length if packgagin else 0.0
                volume_real = packgagin.length * packgagin.width * \
                              packgagin.height if packgagin else \
                    length * width * height
                vals = {
                    'product_id': line.product_id.id,
                    'qty': quantity
                }
                order_line = [(0, 0, vals)]
                pack_list.append(
                    {
                        'packgaging_id': packgagin.id,
                        'qty': quantity,
                        'extended': False if packgagin else True,
                        'length': round(packgagin.length_with_margin) if packgagin else length,
                        'width': round(packgagin.width_with_margin) if packgagin else width,
                        'height': round(packgagin.height_with_margin) if packgagin else height,
                        'weight': line.product_id.weight,
                        'volume_real': volume_real,
                        'order_line_ids': order_line,
                        'res_width': res_width,
                        'res_height': res_height,
                        'res_length': res_length,
                        'res_volume': res_height * res_length * res_width
                    }
                )
                cant -= 1
        # buscamos los accesorios del pedido
        accesories = rec.mapped('order_line').filtered(
            lambda x: x.product_id.accessory_ok
                      and x.product_id.type == 'product')
        accesory_list = []
        for line in accesories:
            cant = line.product_uom_qty
            accesory = {
                'product_id': line.product_id,
                'qty': cant
            }
            accesory_list.append(accesory)

        # ordenamos buscando la caja más grande
        sorted_pack_list = sorted(pack_list, key=itemgetter('volume_real'),
                                  reverse=True)
        # recorremos todas las cajas que hay y vamos metiendo accesorios siempre
        # quepan
        for pack in sorted_pack_list:
            diff_width = pack.get('res_width')
            diff_height = pack.get('res_height')
            diff_length = pack.get('res_length')
            for acc in accesory_list:
                product = acc.get('product_id')
                cant = acc.get('qty', 0)
                quantity = 0
                while cant > 0:
                    if diff_width >= product.width and \
                            diff_height >= product.height and \
                            diff_length >= product.length:
                        diff_width -= product.width
                        diff_height -= product.height
                        diff_length -= product.length
                        quantity += 1
                    cant -= 1
                if quantity > 0:
                    vals = {
                        'product_id': product.id,
                        'qty': quantity
                    }
                    order_line_ids = pack.get('order_line_ids', [])
                    order_line_ids.append((0, 0, vals))
                    weight_pack = pack.get('weight') + product.weight * quantity
                    new_qty = pack.get('qty') + quantity
                    pack.update({
                        'qty': new_qty,
                        'res_width': pack.get('res_width') - product.width * quantity,
                        'res_height': pack.get('res_height') - product.height * quantity,
                        'res_length': pack.get('res_length') - product.length * quantity,
                        'weight': weight_pack,
                        'volume_real': pack.get('volume_real', 0.0),
                        'order_line_ids': order_line_ids
                    })

                    if acc.get('qty', 0) == quantity:
                        accesory_list.remove(acc)
                    else:
                        diff = acc.get('qty', 0) - quantity
                        acc.update({'qty': diff})
        # si quedan accesorios sin guardar buscamos la mas pequeña y la metemos
        # los accesorios restantes extendiendo la caja, o sea ampliandola
        if accesory_list:
            if sorted_pack_list:
                new_pack_list = sorted(sorted_pack_list,
                                       key=itemgetter('volume_real'))
                if new_pack_list:
                    small_box = new_pack_list[0]
                    order_line_ids = small_box.get('order_line_ids', [])
                    acc_quantity = 0
                    acc_width = 0.0
                    acc_height = 0.0
                    acc_length = 0.0
                    acc_weight = 0.0
                    for acc in accesory_list:
                        product = acc.get('product_id')
                        cant = acc.get('qty', 0)
                        vals = {
                            'product_id': product.id,
                            'qty': cant
                        }
                        order_line_ids.append((0, 0, vals))

                        acc_quantity += cant
                        acc_width += product.width * cant
                        acc_height += product.height * cant
                        acc_length += product.length * cant
                        acc_weight += product.weight * cant

                    small_box_res_width = small_box.get('res_width')
                    small_box_res_height = small_box.get('res_height')
                    small_box_res_length = small_box.get('res_length')

                    small_box_width = small_box.get('width')
                    small_box_height = small_box.get('height')
                    small_box_length = small_box.get('length')
                    small_box_weight = small_box.get('weight')

                    new_width = acc_width - small_box_res_width + \
                                small_box_width if acc_width > small_box_res_width \
                        else small_box_width
                    new_height = acc_height - small_box_res_height + \
                                 small_box_height if acc_height > small_box_res_height \
                        else small_box_height
                    new_length = acc_length - small_box_res_length + \
                                 small_box_length if acc_length > small_box_res_length \
                        else small_box_length
                    new_weight = small_box_weight + acc_weight
                    new_qty = small_box.get('qty', 0) + acc_quantity
                    small_box.update(
                        {
                            'extended': True,
                            'qty': new_qty,
                            'length': round(new_length),
                            'width': round(new_width),
                            'height': round(new_height),
                            'weight': new_weight,
                            'volume_real': new_width * new_height * new_length,
                            'order_line_ids': order_line_ids
                        }
                    )

                volumen = 0
                for p_l in new_pack_list:
                    volumen += p_l.get('volume_real', 0.0)
                    p_l.pop('volume_real')
                    p_l.pop('res_width')
                    p_l.pop('res_height')
                    p_l.pop('res_length')
                    p_l.pop('res_volume')

                return {
                    'variant': variant_1,
                    'volumen': round((volumen / 5000), 2) if volumen else 0.0,
                    'packagings': new_pack_list
                }
            else:
                quantity = 0
                volumen = 0.0
                width = 0.0
                height = 0.0
                length = 0.0
                weight = 0.0
                order_line = []
                accesory_list = []
                for line in accesories:
                    cant = line.product_uom_qty
                    quantity += cant
                    vals = {
                        'product_id': line.product_id.id,
                        'qty': cant
                    }
                    order_line.append((0, 0, vals))
                    accesory = {
                        'product_id': line.product_id,
                        'qty': cant
                    }
                    accesory_list.append(accesory)

                    width += line.product_id.width * cant
                    height += line.product_id.height * cant
                    length += line.product_id.length * cant
                    weight += line.product_id.weight * cant

                # buscamos si existe algun paquete donde cabe la suma de todos los
                # accesorios juntos
                packgagin_acc = self.search_packgagin(width, height, length)
                # si existe algún paquete metemos los accesorios dentro
                if packgagin_acc:
                    width = packgagin_acc.width_with_margin
                    height = packgagin_acc.height_with_margin
                    length = packgagin_acc.length_with_margin

                    pack_list.append(
                        {
                            'packgaging_id': packgagin_acc.id,
                            'qty': quantity,
                            'extended': False if packgagin_acc else True,
                            'length': length,
                            'width': width,
                            'height': height,
                            'weight': weight,
                            'order_line_ids': order_line
                        }
                    )
                    volumen += length * width * height
                else:
                    # si no existe ningun paquete el procedimiento es el siguiente:
                    # buscamos el paquete más grande que haya y empezamos a meter
                    # accesorios uno a uno, si logramos meter todos los accesorios
                    # terminamos, sino con el resto de accesorios que quedo buscamos
                    # el paquete que se le ajusta, si no encuentra un paquete parael
                    # resto entramos en un ciclo recursivo y buscamos el paquete mas
                    # grande y asi sucecivamente
                    self.recursive_packs_accesories(accesory_list,
                                                    pack_list)
                    volumen = 0
                    for p_l in pack_list:
                        width_total = p_l.get('width', 0.0)
                        height_total = p_l.get('height', 0.0)
                        length_total = p_l.get('length', 0.0)
                        volumen += width_total * height_total * length_total
                return {
                    'variant': 1,
                    'volumen': round((volumen / 5000), 2) if volumen else 0.0,
                    'packagings': pack_list
                }

        volumen = 0
        for p_l in sorted_pack_list:
            volumen += p_l.get('volume_real', 0.0)
            p_l.pop('volume_real')
            p_l.pop('res_width')
            p_l.pop('res_height')
            p_l.pop('res_length')
            p_l.pop('res_volume')

        return {
            'variant': variant_1,
            'volumen': round((volumen / 5000), 2) if volumen else 0.0,
            'packagings': sorted_pack_list
        }

    def compute_variant_2(self, rec):
        """
        Variante 2 donde los productos se envían en cajas y los accesorios se
        van acomodando en mientras quepan en las cajas de los prodcutos, empe
        zando por la más pequeña, si recorre todas las cajas y quedan accesorios
        pendientes por meter en caja, entonces busca la mas grande y la extiende
        :param rec:
        :return:
        """
        variant_1 = 2
        pack_list = []
        # buscamos los productos del pedido
        products = rec.mapped('order_line').filtered(
            lambda
                x: not x.product_id.accessory_ok
                   and x.product_id.type == 'product')
        for line in products:
            cant = line.product_uom_qty
            while cant > 0:
                quantity = 1
                width = line.product_id.width
                height = line.product_id.height
                length = line.product_id.length
                # buscamos la caja ideal para el producto
                packgagin = self.search_packgagin(width, height, length)
                res_width = packgagin.width_with_margin - width if packgagin else 0.0
                res_height = packgagin.height_with_margin - height if packgagin else 0.0
                res_length = packgagin.length_with_margin - length if packgagin else 0.0
                volume_real = packgagin.length * packgagin.width * \
                              packgagin.height if packgagin else \
                    length * width * height
                vals = {
                    'product_id': line.product_id.id,
                    'qty': quantity
                }
                order_line = [(0, 0, vals)]
                pack_list.append(
                    {
                        'packgaging_id': packgagin.id,
                        'qty': quantity,
                        'extended': False if packgagin else True,
                        'length': packgagin.length_with_margin if packgagin else length,
                        'width': packgagin.width_with_margin if packgagin else width,
                        'height': packgagin.height_with_margin if packgagin else height,
                        'weight': line.product_id.weight,
                        'volume_real': volume_real,
                        'order_line_ids': order_line,
                        'res_width': res_width,
                        'res_height': res_height,
                        'res_length': res_length,
                        'res_volume': res_height * res_length * res_width
                    }
                )
                cant -= 1
        # buscamos los accesorios del pedido
        accesories = rec.mapped('order_line').filtered(
            lambda
                x: x.product_id.accessory_ok and x.product_id.type == 'product')
        accesory_list = []
        for line in accesories:
            cant = line.product_uom_qty
            accesory = {
                'product_id': line.product_id,
                'qty': cant
            }
            accesory_list.append(accesory)
        # ordenamos buscando la caja más pequeña
        sorted_pack_list = sorted(pack_list, key=itemgetter('volume_real'))
        for pack in sorted_pack_list:
            diff_width = pack.get('res_width')
            diff_height = pack.get('res_height')
            diff_length = pack.get('res_length')
            for acc in accesory_list:
                product = acc.get('product_id')
                cant = acc.get('qty', 0)
                quantity = 0
                while cant > 0:
                    if diff_width >= product.width and \
                            diff_height >= product.height and \
                            diff_length >= product.length:
                        diff_width -= product.width
                        diff_height -= product.height
                        diff_length -= product.length
                        quantity += 1
                    cant -= 1
                if quantity > 0:
                    vals = {
                        'product_id': product.id,
                        'qty': quantity
                    }
                    order_line_ids = pack.get('order_line_ids', [])
                    order_line_ids.append((0, 0, vals))
                    weight_pack = pack.get('weight') + product.weight * quantity
                    new_qty = pack.get('qty') + quantity
                    pack.update({
                        'qty': new_qty,
                        'res_width': pack.get('res_width') - product.width * quantity,
                        'res_height': pack.get('res_height') - product.height * quantity,
                        'res_length': pack.get('res_length') - product.length * quantity,
                        'weight': weight_pack,
                        'volume_real': pack.get('volume_real', 0.0),
                        'order_line_ids': order_line_ids
                    })

                    if acc.get('qty', 0) == quantity:
                        accesory_list.remove(acc)
                    else:
                        diff = acc.get('qty', 0) - quantity
                        acc.update({'qty': diff})
        # si quedan accesorios sin guardar buscamos la mas grande y la metemos
        # los accesorios restantes extendiendo la caja, o sea ampliandola
        if accesory_list:
            if sorted_pack_list:
                new_pack_list = sorted(sorted_pack_list,
                                       key=itemgetter('volume_real'),
                                       reverse=True)
                if new_pack_list:
                    big_box = new_pack_list[0]
                    order_line_ids = big_box.get('order_line_ids', [])
                    acc_quantity = 0
                    acc_width = 0.0
                    acc_height = 0.0
                    acc_length = 0.0
                    acc_weight = 0.0
                    for acc in accesory_list:
                        product = acc.get('product_id')
                        cant = acc.get('qty', 0)
                        vals = {
                            'product_id': product.id,
                            'qty': cant
                        }
                        order_line_ids.append((0, 0, vals))
                        while cant > 0:
                            acc_quantity += 1
                            acc_width += product.width
                            acc_height += product.height
                            acc_length += product.length
                            acc_weight += product.weight
                            cant -= 1

                    big_box_res_width = big_box.get('res_width')
                    big_box_res_height = big_box.get('res_height')
                    big_box_res_length = big_box.get('res_length')

                    big_box_width = big_box.get('width')
                    big_box_height = big_box.get('height')
                    big_box_length = big_box.get('length')
                    big_box_weight = big_box.get('weight')

                    new_width = acc_width - big_box_res_width + big_box_width \
                        if acc_width > big_box_res_width else big_box_width
                    new_height = acc_height - big_box_res_height + big_box_height \
                        if acc_height > big_box_res_height else big_box_height
                    new_length = acc_length - big_box_res_length + big_box_length \
                        if acc_length > big_box_res_length else big_box_length
                    new_weight = big_box_weight + acc_weight
                    new_qty = big_box.get('qty', 0) + acc_quantity

                    big_box.update(
                        {
                            'extended': True,
                            'qty': new_qty,
                            'length': round(new_length),
                            'width': round(new_width),
                            'height': round(new_height),
                            'weight': new_weight,
                            'volume_real': new_width * new_height * new_length,
                            'order_line_ids': order_line_ids
                        }
                    )

                volumen = 0
                for p_l in new_pack_list:
                    volumen += p_l.get('volume_real', 0.0)
                    p_l.pop('volume_real')
                    p_l.pop('res_width')
                    p_l.pop('res_height')
                    p_l.pop('res_length')
                    p_l.pop('res_volume')

                return {
                    'variant': variant_1,
                    'volumen': round((volumen / 5000), 2) if volumen else 0.0,
                    'packagings': new_pack_list
                }
            else:
                quantity = 0
                volumen = 0.0
                width = 0.0
                height = 0.0
                length = 0.0
                weight = 0.0
                order_line = []
                for line in accesories:
                    cant = line.product_uom_qty
                    vals = {
                        'product_id': line.product_id.id,
                        'qty': cant
                    }
                    order_line.append((0, 0, vals))
                    accesory = {
                        'product_id': line.product_id,
                        'qty': cant
                    }
                    accesory_list.append(accesory)
                    while cant > 0:
                        quantity += 1
                        width += line.product_id.width
                        height += line.product_id.height
                        length += line.product_id.length
                        weight += line.product_id.weight
                        cant -= 1

                # buscamos si existe algun paquete donde cabe la suma de todos los
                # accesorios juntos
                packgagin_acc = self.search_packgagin(width, height, length)
                # si existe algún paquete metemos los accesorios dentro
                if packgagin_acc:
                    width = packgagin_acc.width_with_margin
                    height = packgagin_acc.height_with_margin
                    length = packgagin_acc.length_with_margin

                    pack_list.append(
                        {
                            'packgaging_id': packgagin_acc.id,
                            'qty': quantity,
                            'extended': False if packgagin_acc else True,
                            'length': packgagin_acc.length_with_margin if packgagin_acc
                            else length,
                            'width': packgagin_acc.width_with_margin if packgagin_acc
                            else width,
                            'height': packgagin_acc.height_with_margin if packgagin_acc
                            else height,
                            'weight': weight,
                            'order_line_ids': order_line
                        }
                    )
                    volumen += length * width * height
                else:
                    # si no existe ningun paquete el procedimiento es el siguiente:
                    # buscamos el paquete más grande que haya y empezamos a meter
                    # accesorios uno a uno, si logramos meter todos los accesorios
                    # terminamos, sino con el resto de accesorios que quedo buscamos
                    # el paquete que se le ajusta, si no encuentra un paquete parael
                    # resto entramos en un ciclo recursivo y buscamos el paquete mas
                    # grande y asi sucecivamente
                    self.recursive_packs_accesories(accesory_list,
                                                    pack_list)
                    volumen = 0
                    for p_l in pack_list:
                        width_total = p_l.get('width', 0.0)
                        height_total = p_l.get('height', 0.0)
                        length_total = p_l.get('length', 0.0)
                        volumen += width_total * height_total * length_total
                return {
                    'variant': 1,
                    'volumen': round((volumen / 5000), 2) if volumen else 0.0,
                    'packagings': pack_list
                }

        volumen = 0
        for p_l in sorted_pack_list:
            volumen += p_l.get('volume_real', 0.0)
            p_l.pop('volume_real')
            p_l.pop('res_width')
            p_l.pop('res_height')
            p_l.pop('res_length')
            p_l.pop('res_volume')

        return {
            'variant': variant_1,
            'volumen': round((volumen / 5000), 2) if volumen else 0.0,
            'packagings': sorted_pack_list
        }

    def compute_variant_3(self, rec):
        """
        Variante 3 donde los productos se envían solos y los accesorios en una
        caja
        :param rec:
        :return:
        """
        variant_3 = 3
        volumen = 0.0
        pack_list = []
        # buscamos los productos del pedido
        products = rec.mapped('order_line').filtered(
            lambda x: not x.product_id.accessory_ok
                      and x.product_id.type == 'product')
        for line in products:
            # cada unidad de cada producto constituye un paquete con las
            # dimensiones del producto
            cant = line.product_uom_qty
            while cant > 0:
                quantity = 1
                width = line.product_id.width
                height = line.product_id.height
                length = line.product_id.length
                vals = {
                    'product_id': line.product_id.id,
                    'qty': quantity
                }
                order_line = [(0, 0, vals)]
                pack_list.append(
                    {
                        'packgaging_id': False,
                        'qty': quantity,
                        'extended': True,
                        'length': round(line.product_id.length),
                        'width': round(line.product_id.width),
                        'height': round(line.product_id.height),
                        'weight': line.product_id.weight,
                        'order_line_ids': order_line
                    }
                )
                volumen += length * width * height
                cant -= 1

        width = 0.0
        height = 0.0
        length = 0.0
        weight = 0.0
        accesory_list = []
        order_line = []
        # buscamos los accesorios del pedido
        accesories = rec.mapped('order_line').filtered(
            lambda x: x.product_id.accessory_ok
                      and x.product_id.type == 'product')
        if accesories:
            quantity = 0
            for line in accesories:
                cant = line.product_uom_qty
                vals = {
                    'product_id': line.product_id.id,
                    'qty': cant
                }
                order_line.append((0, 0, vals))
                accesory = {
                    'product_id': line.product_id,
                    'qty': cant
                }
                accesory_list.append(accesory)

                quantity += cant
                width += line.product_id.width * cant
                height += line.product_id.height * cant
                length += line.product_id.length * cant
                weight += line.product_id.weight * cant

            # buscamos si existe algun paquete donde cabe la suma de todos los
            # accesorios juntos
            packgagin_acc = self.search_packgagin(width, height, length)
            # si existe algún paquete metemos los accesorios dentro
            if packgagin_acc:
                width = packgagin_acc.width_with_margin
                height = packgagin_acc.height_with_margin
                length = packgagin_acc.length_with_margin

                pack_list.append(
                    {
                        'packgaging_id': packgagin_acc.id,
                        'qty': quantity,
                        'extended': False if packgagin_acc else True,
                        'length': round(packgagin_acc.length_with_margin) if packgagin_acc
                        else length,
                        'width': round(packgagin_acc.width_with_margin) if packgagin_acc
                        else width,
                        'height': round(packgagin_acc.height_with_margin) if packgagin_acc
                        else height,
                        'weight': weight,
                        'order_line_ids': order_line
                    }
                )
                volumen += length * width * height
            else:
                # si no existe ningun paquete el procedimiento es el siguiente:
                # buscamos el paquete más grande que haya y empezamos a meter
                # accesorios uno a uno, si logramos meter todos los accesorios
                # terminamos, sino con el resto de accesorios que quedo buscamos
                # el paquete que se le ajusta, si no encuentra un paquete parael
                # resto entramos en un ciclo recursivo y buscamos el paquete mas
                # grande y asi sucecivamente
                self.recursive_packs_accesories(accesory_list, pack_list)
                volumen = 0
                for p_l in pack_list:
                    width_total = p_l.get('width', 0.0)
                    height_total = p_l.get('height', 0.0)
                    length_total = p_l.get('length', 0.0)
                    volumen += width_total * height_total * length_total
        return {
            'variant': variant_3,
            'volumen': round((volumen / 5000), 2) if volumen else 0.0,
            'packagings': pack_list
        }

    def recursive_packs_accesories(self, accesory_list=[], pack_list=[]):
        width = 0.0
        height = 0.0
        length = 0.0
        weight = 0.0
        quantity = 0
        order_line = []
        for line in accesory_list:
            product_id = line.get('product_id', False)
            cant = line.get('qty', 0)
            quantity += cant
            vals = {
                'product_id': product_id.id,
                'qty': cant
            }
            order_line.append((0, 0, vals))

            width += product_id.width * cant
            height += product_id.height * cant
            length += product_id.length * cant
            weight += product_id.weight * cant

        # buscamos si existe algun paquete donde cabe la suma de todos los
        # accesorios juntos
        packgagin_acc = self.search_packgagin(width, height, length)
        # si existe algún paquete metemos los accesorios dentro
        if packgagin_acc:
            width = packgagin_acc.width_with_margin
            height = packgagin_acc.height_with_margin
            length = packgagin_acc.length_with_margin

            pack_list.append(
                {
                    'packgaging_id': packgagin_acc.id,
                    'qty': quantity,
                    'extended': False if packgagin_acc else True,
                    'length': round(packgagin_acc.length_with_margin) if packgagin_acc else length,
                    'width': round(packgagin_acc.width_with_margin) if packgagin_acc else width,
                    'height': round(packgagin_acc.height_with_margin) if packgagin_acc else height,
                    'weight': weight,
                    'order_line_ids': order_line
                }
            )
            return True
        else:
            big_pack = self.env['product.packaging'].search(
                [
                    ('package_carrier_type', '=', 'pakke'),
                ], order='volume DESC', limit=1)
            width_res = 0.0
            height_res = 0.0
            length_res = 0.0
            weight_res = 0.0
            big_qty = 0.0
            list_products = []
            list_products_to_next_round = []
            for line in accesory_list:
                prod = {}
                products_to_next_round = {}
                product_id = line.get('product_id', False)
                prod.setdefault('product_id', product_id.id)
                prod.setdefault('qty', 0)
                prod.setdefault('parcel', {})
                prod_qty = 0
                products_to_next_round.setdefault('product_id', product_id)
                products_to_next_round.setdefault('qty', 0)
                cant = line.get('qty', 0)
                width_by_prod = 0.0
                height_by_prod = 0.0
                length_by_prod = 0.0
                weight_by_prod = 0.0
                while cant > 0:
                    if width_res <= big_pack.width_with_margin and \
                            height_res <= big_pack.height_with_margin and \
                            length_res <= big_pack.length_with_margin:
                        width_res += product_id.width
                        height_res += product_id.height
                        length_res += product_id.length
                        weight_res += product_id.weight
                        width_by_prod += product_id.width
                        height_by_prod += product_id.height
                        length_by_prod += product_id.length
                        weight_by_prod += product_id.weight
                        big_qty += 1
                        prod_qty += 1
                    else:
                        temp_qty = products_to_next_round['qty'] + 1
                        products_to_next_round.update({'qty': temp_qty})
                    cant -= 1

                if prod_qty:
                    prod.update(dict(qty=prod_qty))
                    parcel = {
                        'width': width_by_prod,
                        'height': height_by_prod,
                        'length': length_by_prod,
                        'weight': weight_by_prod,
                    }
                    prod.update(dict(parcel=parcel))
                    list_products.append(prod)
                if products_to_next_round.get('qty', 0) > 0:
                    list_products_to_next_round.append(products_to_next_round)

            order_line = []
            qty_pack = 0.0
            for l in list_products:
                qty_pack += l.get('qty', 0.0)
                v = {
                    'product_id': l.get('product_id', False),
                    'qty': l.get('qty', 0.0)
                }
                order_line.append((0, 0, v))
            pack_list.append(
                {
                    'packgaging_id': big_pack.id,
                    'qty': qty_pack,
                    'extended': False,
                    'length': round(big_pack.length_with_margin),
                    'width': round(big_pack.width_with_margin),
                    'height': round(big_pack.height_with_margin),
                    'weight': weight_res,
                    'order_line_ids': order_line
                }
            )
            self.recursive_packs_accesories(list_products_to_next_round,
                                            pack_list)

    @api.depends('order_line', 'order_line.product_uom_qty')
    def _compute_packaging_ids(self):
        for rec in self:
            carrier_id = self.env.ref('delivery_pakke.delivery_carrier_pakke')
            if carrier_id.build_package:
                rec.packaging_ids = [(5, 0, 0)]
                variant_1 = self.compute_variant_1(rec)
                # variant_2 = self.compute_variant_2(rec)
                variant_3 = self.compute_variant_3(rec)
                min_vol = variant_1.get('volumen', 0.0)
                variant_to_follow = variant_1
                # if variant_2.get('volumen') < min_vol:
                #     min_vol = variant_2.get('volumen')
                #     variant_to_follow = variant_2
                if variant_3.get('volumen') < min_vol:
                    variant_to_follow = variant_3
                packs = variant_to_follow.get('packagings', [])
                one2many_fields = []
                for pack in packs:
                    one2many_fields.append((0, 0, pack))
                rec.write(dict(packaging_ids=one2many_fields))
            else:
                rec.packaging_ids = [(5, 0, 0)]
                variant_3 = self.compute_variant_3(rec)
                packs = variant_3.get('packagings', [])
                one2many_fields = []
                for pack in packs:
                    one2many_fields.append((0, 0, pack))
                rec.write(dict(packaging_ids=one2many_fields))

    def get_couriers(self, carrier_id):
        if carrier_id:
            def exist_in_tmp(item, temp):
                code = item.get('CourierServiceId', False)
                for t in temp:
                    if code and code == t.get('CourierServiceId', False):
                        return float(t.get('TotalPrice', 0.0))
                return False

            url = carrier_id.url_pakke
            api_key = carrier_id.pakke_api_key
            request = PakkeRequest(url, api_key)
            company_id = self.company_id
            tax = company_id.tax_pakke
            enable_tax_pakke = company_id.enable_tax_pakke
            partner_id = self.partner_id
            ZipCodeFrom = company_id.zip
            ZipCodeTo = partner_id.zip

            list_temp = []
            for pack in self.packaging_ids:
                couriers = request._get_guide_price(ZipCodeFrom, ZipCodeTo,
                                                    pack)
                if 'statusCode' in couriers:
                    return couriers
                else:
                    tmpo = couriers.get('Pakke')
                    tmpo_result = []
                    for t in tmpo:
                        if 'BestOption' in list(t.keys()):
                            tmpo_result.append(t)
                    list_temp.append(tmpo_result)
            result = list_temp[0]
            for res in result:
                res.update({'packages': 1})
                if enable_tax_pakke:
                    total_amount = res.get('TotalPrice', 0.0)
                    percent = round((tax * total_amount) / 100, 2)
                    total_amount += percent
                    res.update(
                        {
                            'TotalPrice': round(total_amount, 2)
                        }
                    )
            for res in result:
                for tmp in list_temp[1:]:
                    exist = exist_in_tmp(res, tmp)
                    if exist:
                        total_price = res.get('TotalPrice', 0.0)
                        pack_qty = res.get('packages', 1)
                        if enable_tax_pakke:
                            percent = round((tax * exist) / 100, 2)
                            exist += percent
                        res.update(
                            {
                                'TotalPrice': round(total_price + exist, 2),
                                'packages': pack_qty + 1
                            }
                        )
                    else:
                        result.remove(res)
                        break
            return result
        else:
            return []


class SaleOrderPackagingLine(models.Model):
    _name = 'sale.order.packaging.line'

    def _default_order_id(self):
        active_id = self.env.context.get('active_id', False)
        return active_id

    order_id = fields.Many2one(
        'sale.order',
        string="Sale order",
        default=_default_order_id
    )
    packgaging_id = fields.Many2one(
        'product.packaging',
        string="Package"
    )
    shipment_id = fields.Many2one(
        'pakke.shipment',
        string="Shipment"
    )
    status_shipment = fields.Selection(
        related="shipment_id.tracking_status"
    )
    name = fields.Char(
        related='packgaging_id.name',
        string='Name'
    )
    length = fields.Float(
        string='Length'
    )
    width = fields.Float(
        string='Width'
    )
    height = fields.Float(
        string='Height'
    )
    weight = fields.Float(
        string='Weight'
    )
    volume = fields.Float(
        string="Volume",
        compute='_compute_volume'
    )
    qty = fields.Integer(
        string="Quantity"
    )
    order_line_ids = fields.One2many(
        'sale.order.packaging.prod.line',
        'package_id',
        string="Order Lines"
    )
    extended = fields.Boolean(
        string='Extended',
        help='Checked if not original package, if are extended dimensions.',
        default=False
    )
    file = fields.Binary(
        related="shipment_id.file"
    )
    label = fields.Char(
        related="shipment_id.label",
        readonly=True
    )

    @api.onchange('length', 'width', 'weight')
    @api.depends('length', 'width', 'weight')
    def _compute_volume(self):
        for rec in self:
            rec.volume = rec.length * rec.height * rec.width


class SaleOrderProductPackLine(models.Model):
    _name = 'sale.order.packaging.prod.line'

    product_id = fields.Many2one(
        'product.product',
        string="Product"
    )
    package_id = fields.Many2one(
        'sale.order.packaging.line'
    )
    qty = fields.Integer(
        string="Qty"
    )
    name = fields.Char(
        related="product_id.name"
    )
