# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import binascii
import io
import PIL.PdfImagePlugin   # activate PDF support in PIL
from PIL import Image
import logging
import requests
import json

HEADERS = {"Content-Type" : "application/json","Accept":"application/json"}
import os
import re

from zeep import Client, Plugin
from zeep.exceptions import Fault

from odoo import _


PAKKE_ERROR_MAP = {
    '1052': "El teléfono del reseller no ha sido verificado. Puedes intentarlo "
            "con otro servicio",
}


class PakkeRequest:
    def __init__(self, url, api_key):
        """ Inicianlizando el servicio de pakket"""
        self.url = url
        self.headers = HEADERS
        self.headers['Authorization'] = api_key

    def convert_to_json(self, data):
        return json.dumps(data)

    def _get_couriers(self):
        """
        Obtnener listado de couriers
        :return: retorna una lista de couriers
        """
        response_dict = {}
        try:
            url = self.url + "Couriers"
            headers = self.headers
            get_couriers = requests.request("GET", url=url, headers=headers)
            status_response = get_couriers.status_code
            response = get_couriers.json()
            if status_response in [401]:
                error = response.get('error')
                response_dict['statusCode'] = error.get('statusCode')
                response_dict['name'] = error.get('name')
                response_dict['message'] = error.get('message')
                response_dict['code'] = error.get('code')
                return response_dict
            else:
                return response
        except Exception as e:
            response_dict['statusCode'] = '000'
            response_dict['name'] = _('No conection to seller.pakke.mx')
            response_dict['message'] = _('No conection to seller.pakke.mx')
            response_dict['code'] = '000'
            return response_dict

    def _get_services(self):
        """
        Obtener listado de servicios
        :return: retorna una lista servicios
        """
        url = self.url + "CourierServices"
        headers = self.headers
        response_dict = {}
        get_couriers = requests.request("GET", url=url, headers=headers)
        status_response = get_couriers.status_code
        response = get_couriers.json()
        if status_response in [401]:
            error = response.get('error')
            response_dict['statusCode'] = error.get('statusCode')
            response_dict['name'] = error.get('name')
            response_dict['message'] = error.get('message')
            response_dict['code'] = error.get('code')
            return response_dict
        else:
            return response

    def _get_guide_price(self, zip_code_from, zip_code_to, product_id):
        """
        Obtener una cotización de guía
        :param zip_code_from: zip_code de la compañia
        :param zip_code_to: zip_code del cliente
        :param product_id: producto
        :return: retorna una lista de couriers con cotización
        """
        response_dict = {}
        try:
            url = self.url + "Shipments/rates"
            headers = self.headers
            response_dict = {}
            data = {
                "ZipCodeFrom": zip_code_from,
                "ZipCodeTo": zip_code_to,
                "Parcel": {
                    "Weight": product_id.weight,
                    "Width": product_id.width,
                    "Height": product_id.height,
                    "Length": product_id.length
                },
                "CouponCode": None,
                "InsuredAmount": None
            }
            data_dumps = self.convert_to_json(data)
            get_couriers = requests.request("POST", url=url, headers=headers,
                                            data=data_dumps)
            status_response = get_couriers.status_code
            response = get_couriers.json()
            if status_response in [412]:
                error = response.get('error')
                response_dict['statusCode'] = error.get('statusCode')
                response_dict['name'] = error.get('name')
                response_dict['message'] = error.get('message')
                response_dict['code'] = error.get('code')
                return response_dict
            else:
                return response
        except Exception as e:
            response_dict['statusCode'] = '000'
            response_dict['name'] = _('No conection to seller.pakke.mx')
            response_dict['message'] = e
            response_dict['code'] = '000'
            return response_dict

    def _create_guide(self, courier, product_id, order_id, company_id,
                      partner_id):
        """
        Crea una guía
        :param courier: Courier seleccionado en la cotización
        :param product_id: Producto a enviar
        :param order_id: Número de pedido para referencia
        :param company_id: Compañía
        :param partner_id: Cliente
        :return: Retorna un diccionario con los datos de la guía
        """
        url = self.url + "Shipments"
        headers = self.headers
        response_dict = {}
        data = {
            "CourierCode": courier.get('CourierCode'),
            "CourierServiceId": courier.get('CourierServiceId'),
            "ResellerReference": order_id.name,
            "Content": "Producto",
            "AddressFrom": {
                "ZipCode": company_id.zip,
                "State": company_id.state_id and company_id.state_id.name or '',
                "City": company_id.city or '',
                "Neighborhood": '',
                "Address1": company_id._get_address(),
                "Address2": "",
                },
                "AddressTo": {
                    "ZipCode": partner_id.zip,
                    "State": partner_id.state_id and partner_id.state_id.name
                             or '',
                    "City": partner_id.city or '',
                    "Neighborhood": '',
                    "Address1": partner_id._get_address(),
                    "Address2": "",
                },
                "Parcel": {
                    "Weight": product_id.weight,
                    "Width": product_id.width,
                    "Height": product_id.height,
                    "Length": product_id.length
                },
                "Sender": {
                    "Name": company_id.name,
                    "Phone1": company_id.phone,
                    "Phone2": "",
                    "Email": company_id.email
                },
                "Recipient": {
                    "Name": partner_id.name,
                    "CompanyName": "",
                    "Phone1": partner_id.phone,
                    "Email": partner_id.email
                }
        }
        data_dumps = self.convert_to_json(data)
        get_couriers = requests.request("POST", url=url, headers=headers,
                                        data=data_dumps)
        status_response = get_couriers.status_code
        response = get_couriers.json()
        if status_response in [400, 412]:
            error = response.get('error')
            response_dict['statusCode'] = error.get('statusCode')
            response_dict['name'] = error.get('name')
            response_dict['message'] = error.get('message')
            response_dict['code'] = error.get('code')
            return response_dict
        else:
            return response

    def _get_guides(self, shipment_id):
        """
        Obtener información detallada de un envío
        :param shipment_id: Id de envío
        :return: Retorna un diccionario con la información de la guía
        """
        url = self.url + "Shipments/" + shipment_id
        headers = self.headers
        response_dict = {}
        get_couriers = requests.request("GET", url=url, headers=headers)
        status_response = get_couriers.status_code
        response = get_couriers.json()
        if status_response in [401, 404]:
            error = response.get('error')
            response_dict['statusCode'] = error.get('statusCode')
            response_dict['name'] = error.get('name')
            response_dict['message'] = error.get('message')
            response_dict['code'] = error.get('code')
            return response_dict
        else:
            return response

    def _get_label(self, shipment_id):
        """
        Obtener etiqueta de una guía
        :param shipment_id: Id de envío
        :return: Retorna un diccionario con la información de la etiqueta
        """
        url = self.url + "Shipments/" + shipment_id + '/label'
        headers = self.headers
        response_dict = {}
        get_couriers = requests.request("GET", url=url, headers=headers)
        status_response = get_couriers.status_code
        response = get_couriers.json()
        if status_response in [401, 404]:
            error = response.get('error')
            response_dict['statusCode'] = error.get('statusCode')
            response_dict['name'] = error.get('name')
            response_dict['message'] = error.get('message')
            response_dict['code'] = error.get('code')
            return response_dict
        else:
            return response

    def get_history_guide(self, shipment_id):
        """
           Obtener historial de una guía
           :param shipment_id: Id de envío
           :return: Retorna un diccionario con histórico de movimientos
           realizados
           """
        url = self.url + "Shipments/" + shipment_id + '/history'
        headers = self.headers
        response_dict = {}
        get_couriers = requests.request("GET", url=url, headers=headers)
        status_response = get_couriers.status_code
        response = get_couriers.json()
        if status_response in [401, 404]:
            error = response.get('error')
            response_dict['statusCode'] = error.get('statusCode')
            response_dict['name'] = error.get('name')
            response_dict['message'] = error.get('message')
            response_dict['code'] = error.get('code')
            return response_dict
        else:
            return response
