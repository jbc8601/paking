# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    phone = fields.Char(size=10)

    def _get_address(self):
        for rec in self:
            fields = []
            if rec.street:
                fields.append(rec.street)
            if rec.street2:
                fields.append(rec.street2)
            if rec.city:
                fields.append(rec.city)
            if rec.state_id:
                fields.append(rec.state_id.name)
            if rec.zip:
                fields.append(rec.zip)
            if rec.country_id:
                fields.append(rec.country_id.name)
            if rec.phone:
                fields.append(rec.phone)
            return ', '.join(fields)
