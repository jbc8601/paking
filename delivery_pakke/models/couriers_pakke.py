# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class CouriersPakke(models.Model):
    _name = 'courier.pakke'

    name = fields.Char(
        string='Name'
    )
    code = fields.Char(
        string='Code'
    )
    active = fields.Boolean(
        string='Active'
    )
    image_1920 = fields.Image(
        string="Logo",
        max_width=1920,
        max_height=1920
    )
    image_128 = fields.Image(
        string="Image 128",
        related="image_1920",
        max_width=128,
        max_height=128,
        store=True
    )
