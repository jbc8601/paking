# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models
from .pakke_request import PakkeRequest


class PakkeShipment(models.Model):
    _name = 'pakke.shipment'

    name = fields.Char(
        string='ShipmentId'
    )
    order_id = fields.Many2one(
        'sale.order',
        'order_id'
    )
    currency_id = fields.Many2one(
        string='Currency',
        readonly=False,
        related='order_id.currency_id'
    )
    reseller_id = fields.Char(
        string='ResellerId'
    )
    owner_id = fields.Char(
        string='OwnerId'
    )
    created_at = fields.Datetime(
        string='CreatedAt'
    )
    expire_at = fields.Datetime(
        string='ExpiresAt'
    )
    courier_name = fields.Char(
        string='CourierName'
    )
    courier_code = fields.Char(
        string='CourierCode'
    )
    courier_service_id = fields.Char(
        string='CourierServiceId'
    )
    courier_service = fields.Char(
        string='CourierService'
    )
    length = fields.Float(
        string='Length'
    )
    width = fields.Float(
        string='Width'
    )
    height = fields.Float(
        string='Height'
    )
    weight = fields.Float(
        string='Weight'
    )
    total_amount = fields.Float(
        string='TotalAmount'
    )
    estimated_delivery_date = fields.Date(
        string="EstimatedDeliveryDate"
    )
    tracking_number = fields.Char(
        string="TrackingNumber"
    )
    tracking_status = fields.Selection(
        [
            ('waiting', 'Guía generada y esperando recolección'),
            ('in_transit', 'Paquete en tránsito'),
            ('on_delivery', 'Paquete en proceso de entrega'),
            ('delivered', 'Paquete entregado'),
            ('returned', 'Paquete devuelto'),
            ('cancelled', '​​Guía cancelada o expirada'),
            ('exception', 'Paquete con ​errores de entrega')
        ],
        string="TrackingStatus"
    )
    status = fields.Selection(
        [
            ('success', 'Envío generado'),
            ('refunded', 'Envío reembolsado'),
            ('refundpending', 'Envío con reembolso pendiente'),
            ('refundfailed', 'Envío con reembolso fallido')
        ]
    )
    file = fields.Binary(
        string="Label"
    )
    label = fields.Char(
        'Label',
        readonly=True
    )
    msg = fields.Char(
        string="Message Error"
    )
    history_ids = fields.Many2many(
        'pakke.shipment.history',
        'shipment_history_rel',
        'shipment_id',
        'history_id',
        string="History"
    )

    def update_all_shipments(self):
        shipments = self.env['pakke.shipment'].search([])
        for rec in shipments.sudo():
            rec.update_status_shipment

    def get_label(self):
        for rec in self.sudo():
            url = self.env.ref('delivery_pakke.delivery_carrier_pakke'). \
                url_pakke
            api_key = self.env.ref('delivery_pakke.delivery_carrier_pakke'). \
                pakke_api_key
            request = PakkeRequest(url, api_key)
            label = request._get_label(rec.name)
            if 'statusCode' not in label:
                rec.file = label.get('data')
                tracking_number = label.get('TrackingNumber')
                rec.label = tracking_number + ".pdf"

    def update_status_shipment(self):
        for rec in self.sudo():
            url = self.env.ref('delivery_pakke.delivery_carrier_pakke').\
                url_pakke
            api_key = self.env.ref('delivery_pakke.delivery_carrier_pakke').\
                pakke_api_key
            request = PakkeRequest(url, api_key)
            guide = request._get_guides(rec.name)
            # se va
            label = request._get_label(rec.name)

            if 'statusCode' in guide:
                rec.msg = guide.get('message')
            else:
                # se va
                reseller_id = guide.get('ResellerId')
                owner_id = guide.get('OwnerId')
                created_at_array = guide.get('CreatedAt').split('T')
                created_at_array[1] = created_at_array[1][0:8]
                created_at = ' '.join(created_at_array)
                expire_at_array = guide.get('ExpiresAt').split('T')
                expire_at_array[1] = expire_at_array[1][0:8]
                expire_at = ' '.join(expire_at_array)
                courier_name = guide.get('CourierName')
                courier_code = guide.get('CourierCode')
                courier_service_id = guide.get('CourierServiceId')
                courier_service = guide.get('CourierService')
                length = guide.get('Parcel').get('Length')
                width = guide.get('Parcel').get('Width')
                height = guide.get('Parcel').get('Height')
                weight = guide.get('Parcel').get('Weight')
                total_amount = guide.get('TotalAmount', 0.0)
                estimated_delivery_date = guide.get('EstimatedDeliveryDate')
                tracking_number = guide.get('TrackingNumber')
                rec.msg = ''
                rec.reseller_id = reseller_id
                rec.owner_id = owner_id
                rec.created_at = created_at
                rec.expire_at = expire_at
                rec.courier_name = courier_name
                rec.courier_code = courier_code
                rec.courier_service_id = courier_service_id
                rec.courier_service = courier_service
                rec.length = length
                rec.width = width
                rec.height = height
                rec.weight = weight
                rec.total_amount = total_amount
                rec.estimated_delivery_date = estimated_delivery_date
                rec.tracking_number = tracking_number
                rec.file = label.get('data')
                rec.label = tracking_number + ".pdf"
                rec.status = guide.get('Status').lower()
                rec.tracking_status = guide.get('TrackingStatus').lower()

                list_history = []
                list_history.append((5, 0, 0))
                history_guide = request.get_history_guide(rec.name)
                if 'statusCode' not in history_guide:
                    for h in history_guide:
                        name = h.get('Details', '')
                        date_array = h.get('Date').split('T')
                        date_array[1] = date_array[1][0:8]
                        date = ' '.join(date_array)
                        location = h.get('Location', {})
                        if location:
                            zip_code = location.get('ZipCode', '')
                            state = location.get('State', '')
                            city = location.get('City', '')
                            coord = location.get('Coordinates', {})
                            lat = coord.get('lat', '')
                            lon = coord.get('lng', '')
                        else:
                            zip_code = ''
                            state = ''
                            city = ''
                            lat = ''
                            lon = ''

                        vals = {
                            'name': name,
                            'date': date,
                            'zip_code': zip_code,
                            'state': state,
                            'city': city,
                            'lat': lat,
                            'lon': lon
                        }
                        list_history.append((0, 0, vals))
                rec.history_ids = list_history


class PakkeShipmentHistory(models.Model):
    _name = 'pakke.shipment.history'
    _order = 'date ASC'

    name = fields.Char(
        stirng="Details"
    )
    date = fields.Datetime(
        string='Date'
    )
    zip_code = fields.Char(
        string='ZipCode'
    )
    state = fields.Char(
        string='State'
    )
    city = fields.Char(
        string='City'
    )
    lat = fields.Char(
        string='Latitude'
    )
    lon = fields.Char(
        string='Longitude'
    )
