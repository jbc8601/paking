# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    packaging_ids = fields.Many2many(
        'sale.order.packaging.line',
        string='Pack',
        related='sale_id.packaging_ids'
    )
    shipment_ids = fields.One2many(
        'pakke.shipment',
        'order_id',
        related='sale_id.shipment_ids',
        string='Shipments'
    )
