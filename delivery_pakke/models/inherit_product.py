# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def _get_default_weight_uom(self):
        return self._get_weight_uom_name_from_ir_config_parameter()

    def _get_default_volume_uom(self):
        return self._get_volume_uom_name_from_ir_config_parameter()

    def _get_default_hlw_uom(self):
        return self._get_hlw_uom_name_from_ir_config_parameter()

    accessory_ok = fields.Boolean(
        string='Accessory'
    )
    height = fields.Float(
        string='Height'
    )
    width = fields.Float(
        string='Width'
    )
    length = fields.Float(
        string='Length'
    )
    volume = fields.Float(
        string='Volume',
        compute='_compute_volume',
        inverse='_set_volume',
        digits='Volume',
        store=True
    )
    volume_uom_name = fields.Char(
        string='Volume unit of measure label',
        compute='_compute_volume_uom_name',
        default=_get_default_volume_uom
    )
    hlw_uom_name = fields.Char(
        string='Heiht, Width and Length unit of measure label',
        compute='_compute_hlw_uom_name',
        default=_get_default_hlw_uom
    )
    weight_uom_name = fields.Char(
        string='Weight unit of measure label',
        compute='_compute_weight_uom_name',
        readonly=True,
        default=_get_default_weight_uom
    )

    def _compute_volume_uom_name(self):
        for template in self:
            template.volume_uom_name = \
                self._get_volume_uom_name_from_ir_config_parameter()

    def _compute_weight_uom_name(self):
        for template in self:
            template.weight_uom_name = \
                self._get_weight_uom_name_from_ir_config_parameter()

    def _compute_hlw_uom_name(self):
        for template in self:
            template.hlw_uom_name = \
                self._get_hlw_uom_name_from_ir_config_parameter().display_name

    @api.model
    def _get_weight_uom_name_from_ir_config_parameter(self):
        return self._get_weight_uom_id_from_ir_config_parameter().display_name

    @api.model
    def _get_weight_uom_id_from_ir_config_parameter(self):
        return self.env.ref('uom.product_uom_kgm')

    @api.model
    def _get_hlw_uom_name_from_ir_config_parameter(self):
        return self.env.ref('uom.product_uom_cm')

    @api.model
    def _get_volume_uom_name_from_ir_config_parameter(self):
        return "cm³"

    @api.depends('product_variant_ids', 'product_variant_ids.volume')
    def _compute_volume(self):
        unique_variants = self.filtered(
            lambda template: len(template.product_variant_ids) == 1)
        for template in unique_variants:
            template.volume = template.product_variant_ids.volume
        for template in (self - unique_variants):
            template.volume = 0.0

    def _set_volume(self):
        for template in self:
            if len(template.product_variant_ids) == 1:
                template.product_variant_ids.volume = template.volume


class Product(models.Model):
    _inherit = 'product.product'

    volume = fields.Float(
        string='Volume',
        compute='_compute_volume',
        store=True
    )

    @api.onchange('height', 'width', 'length')
    @api.depends('height', 'width', 'length')
    def _compute_volume(self):
        for rec in self:
            rec.volume = rec.height * rec.length * rec.width
