odoo.define('delivery_pakke.checkout', function (require) {
'use strict';

var core = require('web.core');
var publicWidget = require('web.public.widget');
var QWeb = core.qweb;
var Dialog = require('web.Dialog');

var _t = core._t;
var concurrency = require('web.concurrency');
var dp = new concurrency.DropPrevious();

var template = '' +
    '<templates><t t-name="courier_list_template" >\n' +
    '        <div  class="col-12" id="couriers_list">\n' +
    '            <t t-set="index" t-value="0" />\n' +
    '                <t t-foreach="couriers" t-as="courier">\n' +
    '                    <t t-set="index" t-value="index+1"/>\n' +
    '                    <div  class="card py-2 mb-2 courierRate courier_li" style="border-radius: .25rem;">\n' +
    '                        <div  class="card-boy">\n' +
    '                            <div  class="d-flex w-100 align-items-center">\n' +
    '                                <input t-att-value="courier[\'service_id\']" t-att-courier_code="courier[\'code\']" t-att-courier_name="courier[\'name\']" t-att-total="courier[\'price\']" t-attf-id="courier_#{courier[\'service_id\']}" type="radio" name="courier_type" t-att-checked="index == 1 ? \'checked\' : undefined" />\n' +
    '                                <div >\n' +
    '                                    <img  class="ml-3 courierLogo" t-attf-src="/web/image/courier.pakke/#{courier[\'id\']}/image_128"/>\n' +
    '                                </div>\n' +
    '                                <div  class="my-0 ml-3 mr-auto">\n' +
    '                                    <p  class="my-0"><t t-esc="courier[\'name\']" /> </p>\n' +
    '                                    <p  class="my-0 disabled">\n' +
    '                                        <small ><t t-esc="courier[\'delivery_days\']" />' +
    '                                        </small>\n' +
    '                                    </p>\n' +
    '                                    <p  class="my-0 disabled">\n' +
    '                                        <small ><t t-esc="courier[\'packages\']" /> paquetes' +
    '                                        </small>\n' +
    '                                    </p>\n' +
    '                                </div>\n' +
    '                                <div  class="card-title">\n' +
    '                                    <h5  class="mr-3 my-0"> ' +
    '                                        <t t-if="courier[\'before\']">' +
    '                                            <t t-esc="courier[\'currency\']" /> <t t-esc="courier[\'price\']" />' +
    '                                        </t>' +
    '                                        <t t-else="">' +
    '                                            <t t-esc="courier[\'price\']" /> <t t-esc="courier[\'currency\']" />' +
    '                                        </t>' +
    '                                    </h5>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </t>\n' +
    '            <!--</t>-->\n' +
    '        </div>\n' +
    '    </t>' +
    '</templates>'

publicWidget.registry.websiteSaleDelivery.include({

    events: {
        'change select[name="shipping_id"]': '_onSetAddress',
        'click #delivery_carrier .o_delivery_carrier_select': '_onCarrierClick',
        'click div.courier_li': '_onCourierClick',
    },

    _onCourierClick: function (ev) {
        var $radio = $(ev.currentTarget).find('input[type="radio"]');
        $radio.prop("checked", true);
    },

    /**
     * @override
     */
    start: function () {
        // var prom = this._super.apply(this, arguments);
        var self = this;
        var $carriers = $('#delivery_carrier input[name="delivery_type"]');
        // Workaround to:
        // - update the amount/error on the label at first rendering
        // - prevent clicking on 'Pay Now' if the shipper rating fails
        if ($carriers.length > 0) {
            $carriers.filter(':checked').click();
        }

        // Asynchronously retrieve every carrier price
        _.each($carriers, function (carrierInput, k) {
            self._showLoading($(carrierInput));
            self._rpc({
                route: '/shop/carrier_rate_shipment/type',
                params: {
                    'carrier_id': carrierInput.value,
                    'carrier_type': $(carrierInput).attr('delivery_type'),
                },
            }).then(self._handleCarrierUpdateResultBadge.bind(self));
        });

        return Promise.resolve();
    },
     /**
     * @override
     */
    _handleCarrierUpdateResultBadge: function (result) {
        var $carrierBadge = $('#delivery_carrier input[name="delivery_type"][value=' + result.carrier_id + '] ~ .o_wsale_delivery_badge_price');
        var $courier = $('#delivery_carrier input[name="delivery_type"][value=' + result.carrier_id + '] ~ .label-optional-courier');
        if (result.status === true) {
             // if free delivery (`free_over` field), show 'Free', not '$0'
             if (result.is_free_delivery) {
                 $carrierBadge.text(_t('Free'));
             } else {
                 $carrierBadge.html(result.new_amount_delivery);
                 $courier.html(result.courier);
             }
             $carrierBadge.removeClass('o_wsale_delivery_carrier_error');
        } else {
            $carrierBadge.addClass('o_wsale_delivery_carrier_error');
            $carrierBadge.text(result.error_message);
        }
    },
    /**
     * @override
     */
    _onCarrierClick: function (ev) {
        var $radio = $(ev.currentTarget).find('input[type="radio"]');
        var self = this;
        self.carrier_id = $radio.val();
        this._showLoading($radio);
        $radio.prop("checked", true);
        var $payButton = $('#o_payment_form_pay');
        $payButton.prop('disabled', true);
        $payButton.data('disabled_reasons', $payButton.data('disabled_reasons') || {});
        $payButton.data('disabled_reasons').carrier_selection = true;
        dp.add(this._rpc({
            route: '/carrier_delivery_type',
            params: {
                carrier_id: $radio.val(),
            },
        })).then(function (result) {
           if ('statusCode' in result){
               result = {
                       'status': false,
                       'carrier_id': self.carrier_id,
                       'error_message': result['message']
                   }
                   self._handleCarrierUpdateResultBadge(result);
           }
           else if (result['delivery_type'] == "pakke"){
               if (result['couriers'].length >= 1){
                   QWeb.add_template(template);
                   var couriers = $(QWeb.render('courier_list_template', {
                       'couriers': result['couriers'],
                   }));
                   couriers.appendTo($radio.parent());
                   var dialog = new Dialog(this, {
                        size: 'medium',
                        title:  _t("Select Courier"),
                        $content: couriers,
                        buttons: [{
                            text: _t("Select"),
                            classes : "btn-primary",
                            click: function () {
                                var $input = this.$content.find('input[type="radio"]:checked');
                                var $payButton = $('#o_payment_form_pay');
                                $payButton.data('disabled_reasons').carrier_selection = false;
                                $payButton.prop('disabled', _.contains($payButton.data('disabled_reasons'), true))
                                dp.add(self._rpc({
                                    route: '/shop/update_carrier/pakke',
                                    params: {
                                        carrier_id: $radio.val(),
                                        courier_id: $input.val(),
                                        total_price: $input.attr('total'),
                                        courier_name: $input.attr('courier_name'),
                                        courier_code: $input.attr('courier_code'),
                                    },
                                })).then(self._handleCarrierUpdateResult.bind(self));
                            },
                            close:true,
                        }],
                    });
                   dialog.opened().then(function () {
                        dialog.$('div.courier_li').click(function (ev) {
                            var $radio = $(ev.currentTarget).find('input[type="radio"]');
                            $radio.prop("checked", true);
                        });
                    });
                   dialog.open();
               }
               else{
                   result = {
                       'status': false,
                       'carrier_id': self.carrier_id,
                       'error_message': _t('No couriers available')
                   }
                   self._handleCarrierUpdateResultBadge(result);
               }

           }
           else {
               dp.add(self._rpc({
                route: '/shop/update_carrier',
                params: {
                    carrier_id: $radio.val(),
                },
            })).then(self._handleCarrierUpdateResult.bind(self));
           }
        });
    },
});
});
