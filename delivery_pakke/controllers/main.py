# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _, tools
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.exceptions import ValidationError


class WebsiteSaleDelivery(WebsiteSale):

    @http.route(['/carrier_delivery_type'], type='json', auth='public',
                methods=['POST'], website=True, csrf=False)
    def carrier_delivery_type(self, **post):
        carrier_id = int(post['carrier_id'])
        order = request.website.sale_get_order()
        Courier = request.env['courier.pakke'].sudo()
        Carrier = request.env['delivery.carrier'].sudo()
        list_couriers = []
        couriers = []
        values = {
            'delivery_type': False,
            'couriers': couriers
        }
        if carrier_id:
            carrier_id = int(carrier_id)
            carrier_obj = Carrier.browse(carrier_id)
            if carrier_obj.delivery_type == 'pakke':
                couriers = order.sudo().get_couriers(carrier_obj)
                if not isinstance(couriers, list):
                    return couriers
                for cou in couriers:
                    CourierCode = cou.get('CourierCode', False)
                    if CourierCode:
                        cou_obj = Courier.search(
                            [('code', '=', CourierCode)], limit=1)
                        if cou_obj:
                            val = {
                                'id': cou_obj.id,
                                'service_id': cou.get('CourierServiceId', ''),
                                'name': cou.get('CourierServiceName', ''),
                                'code': cou.get('CourierCode', ''),
                                'price': cou.get('TotalPrice'),
                                'delivery_days': cou.get('DeliveryDays', ''),
                                'packages': cou.get('packages', ''),
                                'currency': order.currency_id.symbol,
                                'before': True if order.currency_id.position
                                                  == 'before' else False
                            }
                            list_couriers.append(val)

                values.update({
                    'delivery_type': carrier_obj.delivery_type or False,
                    'couriers': list_couriers
                })
            else:
                values.update({
                    'delivery_type': carrier_obj.delivery_type or False,
                })
        return values

    @http.route(['/shop/update_carrier/pakke'], type='json', auth='public',
                methods=['POST'], website=True, csrf=False)
    def update_eshop_carrier_pakke(self, **post):
        order = request.website.sale_get_order()
        carrier_id = int(post['carrier_id'])
        total_price = float(post['total_price'])
        courier_id = post['courier_id']
        courier_name = post['courier_name']
        courier_code = post['courier_code']
        n = [courier_code, courier_name]
        if order:
            order.courier_id = courier_id
            order.courier_code = courier_code
            order.total_price_courier = total_price
            order.courier_name = ' '.join(n)
            order._check_carrier_quotation(force_carrier_id=carrier_id)
        return self._update_website_sale_delivery_return2(order, **post)

    @http.route(['/shop/carrier_rate_shipment/type'], type='json', auth='public',
                methods=['POST'], website=True)
    def cart_carrier_rate_shipment_type(self, carrier_id, **kw):
        order = request.website.sale_get_order(force_create=True)
        assert int(
            carrier_id) in order.sudo()._get_delivery_methods().ids, \
            "unallowed carrier"
        Monetary = request.env['ir.qweb.field.monetary'].sudo()
        carrier_type = kw.get('carrier_type', False)
        Carrier = request.env['delivery.carrier'].sudo()
        if carrier_type == 'pakke':
            return {
                'success': False,
                'carrier_id': carrier_id,
                'price': 0.0,
                'error_message': _('Press the delivery method to list the '
                                   'couriers.'),
                'warning_message': False
            }
        res = {'carrier_id': carrier_id}
        carrier = Carrier.browse(int(carrier_id))
        rate = carrier.sudo().rate_shipment(order)
        if rate.get('success'):
            res['status'] = True
            res['new_amount_delivery'] = Monetary.value_to_html(rate['price'], {
                'display_currency': order.currency_id})
            res['is_free_delivery'] = not bool(rate['price'])
            res['error_message'] = rate['warning_message']
        else:
            res['status'] = False
            res['new_amount_delivery'] = Monetary.value_to_html(0.0, {
                'display_currency': order.currency_id})
            res['error_message'] = rate['error_message']
        return res

    def _update_website_sale_delivery_return2(self, order, **post):
        Monetary = request.env['ir.qweb.field.monetary']
        carrier_id = int(post['carrier_id'])
        currency = order.currency_id
        if order:
            return {
                'status': order.delivery_rating_success,
                'error_message': order.delivery_message,
                'carrier_id': carrier_id,
                'is_free_delivery': not bool(order.amount_delivery),
                'new_amount_delivery': Monetary.value_to_html(
                    order.amount_delivery, {'display_currency': currency}),
                'new_amount_untaxed': Monetary.value_to_html(
                    order.amount_untaxed, {'display_currency': currency}),
                'new_amount_tax': Monetary.value_to_html(
                    order.amount_tax, {'display_currency': currency}),
                'new_amount_total': Monetary.value_to_html(
                    order.amount_total, {'display_currency': currency}),
                'courier': order.courier_name
            }
        return {}

    @http.route(['/shop/checkout'], type='http', auth="public", website=True,
                sitemap=False)
    def checkout(self, **post):
        order = request.website.sale_get_order()
        order._compute_packaging_ids()
        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
            return request.redirect('/shop/address')

        for f in self._get_mandatory_billing_fields():
            if not order.partner_id[f]:
                return request.redirect(
                    '/shop/address?partner_id=%d' % order.partner_id.id)

        values = self.checkout_values(**post)

        if post.get('express'):
            return request.redirect('/shop/confirm_order')

        values.update({'website_sale_order': order})

        # Avoid useless rendering if called in ajax
        if post.get('xhr'):
            return 'ok'
        return request.render("website_sale.checkout", values)

    def checkout_form_validate(self, mode, all_form_values, data):
        # mode: tuple ('new|edit', 'billing|shipping')
        # all_form_values: all values before preprocess
        # data: values after preprocess
        error = dict()
        error_message = []

        # Required fields from form
        required_fields = [f for f in (all_form_values.get('field_required')
                                       or '').split(',') if f]
        # Required fields from mandatory field function
        required_fields += mode[1] == 'shipping' and \
                           self._get_mandatory_shipping_fields() \
                           or self._get_mandatory_billing_fields()
        # Check if state required
        country = request.env['res.country']
        if data.get('country_id'):
            country = country.browse(int(data.get('country_id')))
            if 'state_code' in country.get_address_fields() and country.state_ids:
                required_fields += ['state_id']

        # error message for empty required fields
        for field_name in required_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'

        # email validation
        if data.get('email') and not tools.single_email_re.match(
                data.get('email')):
            error["email"] = 'error'
            error_message.append(_('Invalid Email! Please enter a valid '
                                   'email address.'))

        # phone validation
        if data.get('phone') and len(data.get('phone')) != 10 or \
                not data.get('phone').isdigit():
            error["phone"] = 'error'
            error_message.append(
                _('Invalid Phone! Please enter a valid phone number, '
                  'remember are 10 digits.'))

        # vat validation
        Partner = request.env['res.partner'].sudo()
        if data.get("vat") and hasattr(Partner, "check_vat"):
            if data.get("country_id"):
                data["vat"] = Partner.fix_eu_vat_number(
                    data.get("country_id"), data.get("vat"))
            partner_dummy = Partner.new({
                'vat': data['vat'],
                'country_id': (int(data['country_id'])
                               if data.get('country_id') else False),
            })
            try:
                partner_dummy.check_vat()
            except ValidationError:
                error["vat"] = 'error'

        if [err for err in error.values() if err == 'missing']:
            error_message.append(_('Some required fields are empty.'))

        return error, error_message
