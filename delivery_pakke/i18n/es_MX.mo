��    O      �  k         �  	   �     �     �     �  <   �          6  	   ;     E  @   M     �     �     �  	   �     �     �     �  
   �  
   �  =      	   >     H     U     f  	   |     �     �  `   �  	   �  2   	  I   4	     ~	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
  
   &
  "   1
     T
     h
     �
  /   �
     �
     �
     �
     �
     �
     �
                      %   -     S  
   \  	   g     q     �     �  	   �     �     �     �     �     �     �     �     
          .    4  	   K     U     \     o  J   }     �     �     �     �  I        L     _     s     �     �     �     �  
   �  	   �  I   �     5     >     N     l     �     �     �  {   �  
   '  H   2  g   {     �     �     	     $     >     G     X  %   _     �     �     �     �     �     �  $   �       #     	   A  >   K     �     �     �     �  	   �     �     �     �  
          /   '     W     ]     i     p     �     �     �     �     �     �     �     �     �  '   �     %  $   *     O         O   -       (   4   2   "           K   6       ?       %                             9   >   @             <      F   ,   I       .   3               N   =         J                G          $   H   /      1       A                        7      +              	   B       )   !         :   E   D      &   ;       5      C           #      M   
         *   0         8      L          '    Accessory Active Build package Carrier Checked if not original package, if are extended dimensions. Choose a delivery method Code Companies Contact Courier Id choiced in quotation and necessary to create a guide. Courier Name Courier code CourierCode CourierId CourierName CourierService CourierServiceId Created by Created on Delivery will be updated after choosing a new delivery method Delivery: Display Name Enable Tax Pakke EstimatedDeliveryDate ExpiresAt Extended Height If it is checked, the packages are built dynamically, if not, the products are sent as they are. Image 128 Invalid Email! Please enter a valid email address. Invalid Phone! Please enter a valid phone number, remember are 10 digits. Label Last Modified on Last Updated by Last Updated on Length Message Error Name No conection to seller.pakke.mx No couriers available Order Lines OwnerId Pack Package Packagings Packagings ready to send to Pakke. Pakke Configuration Pakke Tax Configuration Phone Press the delivery method to list the couriers. Product Product Packaging Product Template Products in package Provider Qty Quantity Sales Order Select Select Courier Select the courier to calculate cost. Shipment ShipmentId Shipments Shipping Methods Some required fields are empty. Status Tax Pakke Total Price Courier TotalAmount Transfer Units Url Volume Volume unit of measure label Weight Weight unit of measure label Width Project-Id-Version: Odoo Server 13.0+e
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-08-27 19:58-0400
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
X-Generator: Poedit 2.3
 Accesorio Activo Construir Paquetes Transportista Marcado si no es un paquete original, si las dimensiones fueron ampliadas. Elegir un método de entrega Código Compañías Contacto Id del Courier seleccionado en el pedido y necesario para crear la guía. Nombre del Courier Código del Courier Código del Courier Id del Courier Nombre del Courier Servicio del Courier Id Servicio del Courier Creado por Creado el La entrega se actualizará después de elegir un nuevo método de entrega Entrega: Nombre mostrado Habilitar Impuesto para Pakke Fecha de entrega estimada Fecha de expiración Ampliado Altura Está marcado si los paquetes son construidos dinamicamente, si no, los productos son enviados con sus propias dimensiones. Imagen 128 Correo Inválido! Por favor introduzca una dirección de correo válida. Teléfono Inválido! Por favor introduzca un número de teléfono valido, recuerde que son 10 dígitos. Descripción Última modificación en Última actualización por Última actualización el Longitud Mensaje de Error Nombre No hay conección con seller.pakke.mx No hay couriers disponibles Líneas del pedido Id del propietario Paquete Paquete Paquetes Paquetes listos para enviar a Pakke. Configuración de Pakke Configuración de impuesto de Pakke Teléfono Presione el método de entrega Pakke para listar los couriers. Producto Empaquetado del producto Plantilla de producto Productos en el paquete Proveedor Cantidad Cantidad Pedido de venta Seleccione Seleccione el Courier Seleccione el Courier para calcular los costos. Guía Id de Guía Guías Métodos de envío Algunos campos están vacíos. Estado Impuesto Pakke Precio Total del Courier Importe Total Albarán Unidades URL Volumen Etiqueta de unidad de medida de volumen Peso Etiqueta de Unidad de Medida de peso Ancho 